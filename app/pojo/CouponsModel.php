<?php
class CouponsModel extends Db {
	protected $_coupons_history = 'w_coupons_history'; // 操作记录
	protected $_member_list = 'w_member_list';
	protected $_member_group = 'w_member_group';
	protected $_action_log = 'w_action_log'; // 操作记录
	protected $_money_history = 'w_money_history'; // 操作记录
	protected $_face_list = 'w_face_list';
	protected $_coupons_audit = 'w_coupons_audit'; //日结算
	

	/**
	 * 
	 * 更新积分值
	 * @param unknown_type $uid
	 * @param unknown_type $coupons  积分
	 * @param unknown_type $remark 渠道
	 * @param unknown_type $paymment 0收入 1支出
	 * @param unknown_type $chargetypes  充值 消费 奖励
	 */
	public function saveCoupons($uid, $username, $coupons, $remark, $payment, $chargetypes, $orderno, $status) {
		return $this->add ( $this->_coupons_history, array ('uid' => $uid, 'username' => $username, 'coupons' => $coupons, 'date' => time (), 'payment' => $payment, 'chargetypes' => $chargetypes, 'remark' => $remark, 'orderno' => $orderno, 'status' => $status ) );
	}
	public function getSumCouponsByUid($uid) {
		$sql = "SELECT sum(coupons) total FROM w_coupons_history WHERE uid=$uid AND status IN (1,8)";
		return $this->fetch ( $sql );
	}
	
	public function distinctUid() {
		$sql = "select DISTINCT(uid) from $this->_coupons_history";
		return $this->fetchAll ( $sql );
	}
	/**
	 * 设置jife
	 * */
	public function setCouponsStatus($v, $where) {
		$this->update ( $this->_coupons_history, $v, $where );
	}
	
	/**
	 * 
	 * 指定时间收入
	 * */
	public function getIncomeByTime($starttime, $endtime, $where) {
		$where = $this->batchWhere ( $where );
		$sql = "select sum(coupons) income from w_coupons_history a WHERE payment=0 AND   `date`>=$starttime and `date`<=$endtime AND $where";
		return $this->fetch ( $sql );
	}
	/**
	 * 指定时间支出
	 * Enter description here ...
	 * @param unknown_type $endtime
	 * @param unknown_type $where
	 */
	public function getSpendByTime($starttime, $endtime, $where) {
		$where = $this->batchWhere ( $where );
		$sql = "select sum(coupons) spend from w_coupons_history a WHERE payment=1 AND  `date`>=$starttime and `date`<=$endtime AND $where";
		return $this->fetch ( $sql );
	}
	/**
	 * 期初收入
	 * Enter description here ...
	 * @param unknown_type $endtime
	 * @param unknown_type $where
	 */
	public function getIncomeByBeforeTime($endtime, $where) {
		$where = $this->batchWhere ( $where );
		$sql = "select sum(coupons) income from w_coupons_history a WHERE  payment=0 AND `date`<=$endtime AND $where";
		return $this->fetch ( $sql );
	}
	/**
	 * 期初支出
	 * Enter description here ...
	 * @param unknown_type $endtime
	 * @param unknown_type $where
	 */
	public function getSpendByBeforeTime($endtime, $where) {
		$where = $this->batchWhere ( $where );
		$sql = "select sum(coupons) spend from w_coupons_history a WHERE payment=1 AND   `date`<=$endtime AND $where";
		return $this->fetch ( $sql );
	}
	/**
	 * 获取充值记录
	 * Enter description here ...
	 * @param unknown_type $where
	 */
	public function getCouponsHistory($where = NULL) {
		return $this->getAll ( $this->_coupons_history, $where );
	}
	public function getCouponsHistoryPage($start, $num, $where = NULL) {
		if ($where != null) {
			$where = $this->batchWhere ( $where );
			$sql = "SELECT * FROM $this->_coupons_history  WHERE $where ORDER BY id DESC LIMIT $start,$num";
		} else {
			$sql = "SELECT * FROM $this->_coupons_history  ORDER BY id DESC LIMIT $start,$num";
		}
		
		return $this->fetchAll ( $sql );
	}
	
	public function countCouponsHisotry($where) {
		if (! empty ( $where )) {
			$where = $this->batchWhere ( $where );
			$sql = "SELECT count(id) num FROM $this->_coupons_history WHERE $where";
		} else {
			$sql = "SELECT count(id) num FROM $this->_coupons_history";
		}
		
		$rs = $this->fetch ( $sql );
		return $rs ['num'];
	}
	
	public function getLastAudit() {
		//统计当前用户账户总额  获取最新的一条记录
		$sql = "select * from $this->_coupons_audit where id=(select MAX(id) FROM w_coupons_audit)";
		return $this->fetch ( $sql );
	}
	
	/**
	 * 查找未确认订单之后有已经确认的订单，不允许结算
	 * Enter description here ...
	 * @param int $id
	 */
	public function getCouponsById($couponsId) {
		$sql = "SELECT * FROM $this->_coupons_history WHERE  status=8 AND id>$couponsId LIMIT 1";
		return $this->fetch ( $sql );
	}
	
	/**
	 * 获取最新的积分的积分
	 * Enter description here ...
	 */
	public function getMaxCoupons() {
		$sql = "select max(id) id from $this->_coupons_history WHERE status=8";
		return $this->fetch ( $sql );
	}
	
	public function getMinUnConfirmedId() {
		$sql = "select min(id) id from $this->_coupons_history WHERE status=1";
		return $this->fetch ( $sql );
	}
	public function getAudit($limit = 30) {
		return $this->getAll ( $this->_coupons_audit, null, null, 'id DESC', $limit );
	}
	public function getLastIncome($id) {
		$sql = "SELECT sum(coupons) income FROM $this->_coupons_history WHERE payment=0 AND status=8 AND id>$id";
		return $this->fetch ( $sql );
	}
	/**
	 * 新增参数
	 * Enter description here ...
	 * @param unknown_type $params
	 */
	public function addAudit($params) {
		$this->add ( $this->_coupons_audit, $params );
		return $this->lastInsertId ();
	}
	public function getLastSpend($id, $status) {
		$sql = "SELECT sum(coupons) spend FROM $this->_coupons_history WHERE payment=1 AND status=$status AND id>$id";
		return $this->fetch ( $sql );
	}
	
	/**
	 * 获取指定时间的订单
	 * @param unknown_type $starttime
	 * @param unknown_type $endtime
	 * @param unknown_type $where
	 */
	public function getCouponsHistoryAll($starttime, $endtime, $where = NULL) {
		if ($where != NULL) {
			
			$where = $this->batchWhere ( $where );
			$sql = "SELECT * FROM $this->_coupons_history  WHERE date>=$starttime AND date<=$endtime AND $where";
		} else {
			$sql = "SELECT * FROM $this->_coupons_history WHERE date>=$starttime AND date<=$endtime ";
		}
		
		return $this->fetchAll ( $sql );
	}
	
	/**
	 * 返回CouponsModel
	 * @return CouponsModel
	 */
	public static function instance() {
		return self::_instance ( __CLASS__ );
	}
}